//
//  CoreDataController.swift
//  To-Do List
//
//  Created by Tran Kim Thach on 10/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import Foundation

import UIKit
import CoreData

class CoreDataController: NSObject, NSFetchedResultsControllerDelegate, DatabaseProtocol {
    var listeners = MulticastDelegate<DatabaseListener>()
    var persistantContainer: NSPersistentContainer
    
    // Results
    var doingTaskResultsController: NSFetchedResultsController<Task>?
    var doneTaskResultsController: NSFetchedResultsController<Task>?
    
    override init() {
        persistantContainer = NSPersistentContainer(name: "TaskModel")
        persistantContainer.loadPersistentStores() { (description, error) in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
        }
        
        super.init()
        
        // Create default tasks here if you want
//        if fetchAllTasks(false).count == 0 {
//            createDefaultTasks()
//        }
    }
    
    func saveContext() {
        if persistantContainer.viewContext.hasChanges {
            do {
                try persistantContainer.viewContext.save()
            } catch {
                fatalError("Failed to save data to Core Data: \(error)")
            }
        }
    }
    
    
    func addTask(title: String, taskDescription: String, dueDate: Date, location: String) -> Task {
        let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: persistantContainer.viewContext) as! Task
        
        task.title = title
        task.dueDate = dueDate as NSDate
        task.location = location
        task.descriptionText = taskDescription
        if (title == "FIT3178 A2") {
            task.done = true
        }
        else {
            task.done = false
        }
        
        // This less efficient than batching changes and saving once at end.
        saveContext()
        return task
    }
    
    func updateTask(task: Task, newTitle: String, newTaskDescription: String, newDueDate: Date, newLocation: String) {
        task.title = newTitle
        task.descriptionText = newTaskDescription
        task.dueDate = newDueDate as NSDate
        task.location = newLocation
        
        saveContext()
        
    }
    
    func toggleDoneTask(task: Task) {
        task.done = !task.done
        saveContext()
    }
    
    
    func deleteTask(task: Task) {
        persistantContainer.viewContext.delete(task)
        // This less efficient than batching changes and saving once at end.
        saveContext()
    }
    
    
    
    func addListener(listener: DatabaseListener) {
        
        listeners.addDelegate(listener)
        // Fetch tasks according to the listener type
        listener.onTaskChange(change: .update, tasks: fetchTasks(listener.listenerType))

        
        
    }
    
    
    func removeListener(listener: DatabaseListener) {
        listeners.removeDelegate(listener)
        
    }
    
    func fetchTasks(_ listenerType: ListenerType) -> [Task] {
        var tasks = [Task]()

        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        // Sort by title, ascending order
        let nameSortDescriptor = NSSortDescriptor(key: "title", ascending: true)

        // If the listener type is done (i.e called from Completed Tasks screen)
        // we look for done tasks
        // And vice versa
        var isDone = false
        if listenerType == ListenerType.done {
            isDone = true
        }
        let predicate = NSPredicate(format: "done == %@", NSNumber(booleanLiteral: isDone))

        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        
        // Each screen has its own ResultsController
        if !isDone {
            if doingTaskResultsController == nil {
                
                doingTaskResultsController =
                    NSFetchedResultsController<Task>(fetchRequest: fetchRequest,
                                                     managedObjectContext: persistantContainer.viewContext, sectionNameKeyPath: nil,
                                                     cacheName: nil)
                doingTaskResultsController?.delegate = self
                do {
                    try doingTaskResultsController?.performFetch()
                } catch {
                    print("Fetch Request failed: \(error)")
                }

            }
            if doingTaskResultsController?.fetchedObjects != nil {
                tasks = (doingTaskResultsController?.fetchedObjects)!
            }
            
        }
        else if isDone {
            if doneTaskResultsController == nil {
                doneTaskResultsController =
                    NSFetchedResultsController<Task>(fetchRequest: fetchRequest,
                                                     managedObjectContext: persistantContainer.viewContext, sectionNameKeyPath: nil,
                                                     cacheName: nil)
                doneTaskResultsController?.delegate = self
                do {
                    try doneTaskResultsController?.performFetch()
                } catch {
                    print("Fetch Request failed: \(error)")
                }
            }
            if doneTaskResultsController?.fetchedObjects != nil {
                tasks = (doneTaskResultsController?.fetchedObjects)!
            }
        }
        
        
        return tasks
    }
    
    // MARK: - Fetched Results Controller Delegate
    func controllerDidChangeContent(_ controller:
        NSFetchedResultsController<NSFetchRequestResult>) {
        // It doesn't matter which controller calls the method, invoke fetchTasks
        // Logic for different listeners is in fetchTasks()
        // I figured this would be more readable
        listeners.invoke {
            (listener) in listener.onTaskChange(change: .update, tasks: fetchTasks(listener.listenerType))
        }

    }
    
    // MARK: - Default entries
    
    func createDefaultTasks() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy HH:mm"
        let date = dateFormatter.date(from: "21/04/20 20:00")!
        
        let _ = addTask(title: "FIT3178 A2", taskDescription: "Build To-Do List", dueDate: date, location: "Monash")
        
        let _ = addTask(title: "FIT3173 A1", taskDescription: "Buffer Overflow", dueDate: date, location: "Monash")
        
        let _ = addTask(title: "FIT3165 A1", taskDescription: "Report", dueDate: date, location: "Monash")
        
    }
}
