//
//  DatabaseProtocol.swift
//  To-Do List
//
//  Created by Tran Kim Thach on 10/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import Foundation

// I only use update
enum DatabaseChange {
    case add
    case delete
    case update
}

enum ListenerType {
    case notDone
    case done
    case all
}


protocol DatabaseListener: AnyObject {
    
    var listenerType: ListenerType {get set}
    func onTaskChange(change: DatabaseChange, tasks: [Task])
}

protocol DatabaseProtocol: AnyObject {
    
    func addTask(title: String, taskDescription: String, dueDate: Date, location: String) -> Task
    func updateTask(task: Task, newTitle: String, newTaskDescription: String, newDueDate: Date, newLocation: String)
    func toggleDoneTask(task: Task)
    func deleteTask(task: Task)
    
    func addListener(listener: DatabaseListener)
    func removeListener(listener: DatabaseListener)
}
