//
//  AddTaskViewController.swift
//  FIT3178-Assignment2-To do List
//
//  Created by Tran Kim Thach on 15/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit

class AddTaskViewController: UIViewController, UITextViewDelegate {
    let grayColor = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
    weak var databaseController: DatabaseProtocol?

    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Customize the UITextView
        descriptionField.delegate = self
        descriptionField.layer.borderColor = grayColor.cgColor
        descriptionField.layer.borderWidth = 0.5
        descriptionField.layer.cornerRadius = 5
        descriptionField.textColor = grayColor
        descriptionField.text = "Description"
        
        // Database config
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        databaseController = appDelegate.databaseController
        
    }
    
    // MARK: - These 2 functions handle behaviors of placeholder text in descriptionField
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Description" && textView.textColor == grayColor)
        {
            textView.text = ""
            textView.textColor = .black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Description"
            textView.textColor = grayColor
        }
        textView.resignFirstResponder()
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        let title: String = titleField.text!
        let description: String = descriptionField.text!
        let location: String = locationField.text!
        // I don't restrict the date to just be from the future
        // Since the user might have a reason to record a past event
        let date: Date = datePicker!.date
        
        // If title is missing
        if (title.isEmpty || description.isEmpty || description == "Description") {
            // Show invalid input alert
            let alert = UIAlertController(title: "", message: "Title and meaningful description are required!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            // Save new task to CoreData
            let _ = databaseController!.addTask(title: title, taskDescription: description, dueDate: date, location: location)
            //Then return to main screen
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        // Return to main screen without saving
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
