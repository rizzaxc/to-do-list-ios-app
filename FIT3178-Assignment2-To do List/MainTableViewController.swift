//
//  MainTableViewController.swift
//  FIT3178-Assignment2-To do List
//
//  Created by Tran Kim Thach on 15/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController, UISearchResultsUpdating, DatabaseListener {
    
    var allCurrentTask: [Task] = []
    var filteredTask: [Task] = []
    weak var databaseController: DatabaseProtocol?
    var isMainScreen = true
    var listenerType = ListenerType.notDone
    let searchController = UISearchController(searchResultsController: nil);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure searchController
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Task"
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        
        // Database Config
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        databaseController = appDelegate.databaseController
        
        // Set the title
        if isMainScreen {
            self.title = "Main Screen"
        }
        else {
            self.title = "Completed Tasks"
            listenerType = ListenerType.done
            navigationItem.rightBarButtonItems?.last?.isEnabled = false
        }

    }
    

    
    // MARK: - Add and remove database listeners
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        databaseController?.addListener(listener: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        databaseController?.removeListener(listener: self)
    }
    

    // MARK: - Table view data source
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // The table has only 1 section
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // If filtering, return the number of items in filterdTask
        if isFiltering() {
            return filteredTask.count
        }
        
        // Otherwise, return count of all tasks
        return allCurrentTask.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! MainTableViewCell
        let task: Task
        if isFiltering() {
            task = filteredTask[indexPath.row]
        }
        else {
            task = allCurrentTask[indexPath.row]
        }
        
        cell.titleLabel.text = task.title
        // If a task in main screen is overdue, its title is red. Default black
        if task.dueDate?.compare(Date()) == ComparisonResult.orderedAscending && isMainScreen {
            cell.titleLabel.textColor = UIColor.red
        }
        else {
            cell.titleLabel.textColor = UIColor.black
        }
        
        // Convert NSDate from task.dueDate to string
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd HH:mm"
        let dateField = dateFormatter.string(from: task.dueDate! as Date)
        cell.dueDateLabel.text = dateField

        
        return cell
        
    }
    
    func setTableFooterView() {
        // Set table footer to display the number of tasks after everything's loaded
        let taskCountLabel = UILabel(frame: CGRect(x: 32, y: 0, width: 0, height: 32))
        if isFiltering() {
            taskCountLabel.text = "Number of tasks: \(filteredTask.count)"
        }
        else {
            taskCountLabel.text = "Number of tasks: \(allCurrentTask.count)"
            
        }
        tableView.tableFooterView = taskCountLabel
    }


    // MARK: - Search functionality. Tutorials include this guide https://www.raywenderlich.com/472-uisearchcontroller-tutorial-getting-started
    // and unit materials
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        // Returns true if the searchController is active & searchBar not empty
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        // All tasks with titles matching the search text is returned to filteredTask
        filteredTask = allCurrentTask.filter({( task : Task) -> Bool in
            return (task.title?.lowercased().contains(searchText.lowercased()))!
        })
        tableView.reloadData()
        setTableFooterView()

    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    // MARK: - Database Listener
    func onTaskChange(change: DatabaseChange, tasks: [Task]) {
        allCurrentTask = tasks
        updateSearchResults(for: navigationItem.searchController!)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailSegue" {
            if let row = tableView.indexPathForSelectedRow?.row {
                let task: Task
                if isFiltering() {
                    task = filteredTask[row]
                }
                else {
                    task = allCurrentTask[row]
                }
                let destination = segue.destination as! TaskDetailViewController
                destination.task = task
            }
        }
        
        if segue.identifier == "showCompletedTaskSegue" {
            let destination = segue.destination as! MainTableViewController
            destination.isMainScreen = false
        }
    }

    @IBAction func unwindToMain(segue:UIStoryboardSegue) {
        
    }


}
