//
//  Task+CoreDataProperties.swift
//  FIT3178-Assignment2-To do List
//
//  Created by Tran Kim Thach on 15/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var title: String?
    @NSManaged public var descriptionText: String?
    @NSManaged public var dueDate: NSDate?
    @NSManaged public var location: String?
    @NSManaged public var done: Bool

}
