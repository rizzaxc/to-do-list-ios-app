//
//  TaskDetailViewController.swift
//  FIT3178-Assignment2-To do List
//
//  Created by Tran Kim Thach on 15/4/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit

class TaskDetailViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var task: Task?
    let grayColor = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
    weak var databaseController: DatabaseProtocol?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Database config
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        databaseController = appDelegate.databaseController

        // Populate the views
        titleField.text = task?.title
        // Customize the TextView
        descriptionField.delegate = self
        descriptionField.layer.borderColor = grayColor.cgColor
        descriptionField.layer.borderWidth = 0.5
        descriptionField.layer.cornerRadius = 5
        descriptionField.text = task?.descriptionText
        // Dynamically fit the content
        modifyTextViewProperties(false)
        
        datePicker.date = task!.dueDate! as Date
        
        locationField.text = task?.location
        
        if task?.done == true {
            doneButton.title = "Put back"
        }
        else {
            doneButton.title = "Done"
        }
    }
    
    // MARK: - These 2 functions handle the logic of editing description textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        // Placeholder text logic
        if (textView.text == "Description" && textView.textColor == grayColor)
        {
            textView.text = ""
            textView.textColor = .black

        }
        modifyTextViewProperties(true)
        
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        // Placeholder text logic
        if (textView.text == "")
        {
            textView.text = "Description"
            textView.textColor = grayColor
        }
        
        modifyTextViewProperties(false)

        textView.resignFirstResponder()
    }
    
    func modifyTextViewProperties(_ isEditing: Bool) {
        // Dynamic sizing for textview. Reference: StackOverflow
        // If user is editing, expand the view and enable scrolling.
        if isEditing {
            descriptionField.isScrollEnabled = true
            descriptionField.translatesAutoresizingMaskIntoConstraints = false
        }
        // Else, disable scrolling, resize the view to fit the content
        else {
            descriptionField.isScrollEnabled = false
            descriptionField.translatesAutoresizingMaskIntoConstraints = true
            descriptionField.sizeToFit()

        }
    }
    
    // MARK: - Button logic
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        let _ = databaseController?.toggleDoneTask(task: task!)
        unwind()

    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        // Invalid input handling
        var msg: String?
        if (titleField.text == "") {
            msg = "Title must not be empty!"
        }
        else if (descriptionField.text == "" || descriptionField.text == "Description") {
            msg = "Meaningful description is required!"
        }


        if let message = msg {
            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        // Update the task
        else {
            
            let _ = databaseController?.updateTask(task: task!, newTitle: titleField.text!, newTaskDescription: descriptionField.text!, newDueDate: datePicker.date, newLocation: locationField.text!)
            
            unwind()
        }

    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        let _ = databaseController?.deleteTask(task: task!)
        unwind()
    }
    
    // Return to the previous screen
    func unwind() {
        performSegue(withIdentifier: "unwindToMainSegue", sender: self)
        
    }

}
